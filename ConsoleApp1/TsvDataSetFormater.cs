﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    /// <summary>
    /// This class contains main TVSwork methods, such as read, format and write result
    /// </summary>
    class TsvDataSetFormater
    {
        private string _sourceFilePath;
        private string _resultFilePath;
        private string _prevTsvFilePath;

        private bool _usePrevTsv;

        private string _currentOperatorClientId;
        private string _currentUserClientId;
        private int _defaultGuidCount;

        private string _defaultColSeparator;
        private string _defaultRowSeparator;
        private string _defaultOutputHeader;

        private int _defaultUp_Votes;
        private int _defaultDown_Votes;
        private AgeOption _defaultAge;
        private GenderOption _defaultGender;
        private string _defaultAccent;

        private int _prevOperatorsPathNumber;
        private int _prevUsersPathNumber;

        /// <summary>
        /// Create a new instance of TsvDataSetFormater with default configeration (for better gonfig use CofigereFormater method)
        /// </summary>
        /// <param name="inputFilePath">Input file formst must be this: [Path (exp. "00.mp3")]\t-\t[sentence]</param>
        /// <param name="outputFilePath"></param>
        public TsvDataSetFormater(string inputFilePath = "D:\\text.txt", string outputFilePath = "D:\\Result.tsv", string prevTsvFilePath = "", int prevOperatorPathNum = -1, int prevUserPathNum = -1)
        {
            _sourceFilePath = inputFilePath;
            _resultFilePath = outputFilePath;
            _prevTsvFilePath = prevTsvFilePath;
            _prevOperatorsPathNumber = prevOperatorPathNum;
            _prevUsersPathNumber = prevUserPathNum;
            _usePrevTsv = (!string.IsNullOrEmpty(prevTsvFilePath) && File.Exists(prevTsvFilePath)) || (prevOperatorPathNum > 0 && prevUserPathNum > 0);

            _defaultGuidCount = 4;
            _defaultColSeparator = "\t";
            _defaultRowSeparator = "\r\n";
            _defaultOutputHeader = "client_id\tpath\tsentence\tup_votes\tdown_votes\tage\tgender\taccent\r\n";

            _defaultUp_Votes = 2;
            _defaultDown_Votes = 0;
            _defaultAge = AgeOption.thirties;
            _defaultGender = GenderOption.male;
            _defaultAccent = string.Empty;

            CreateClientIds();
        }

        public void CreateClientIds()
        {
            _currentOperatorClientId = string.Empty;
            _currentUserClientId = string.Empty;

            for (int i = 0; i < _defaultGuidCount; i++)
            {
                _currentOperatorClientId += Guid.NewGuid().ToString().Replace("-", string.Empty);
                _currentUserClientId += Guid.NewGuid().ToString().Replace("-", string.Empty);
            }
        }

        public void ConfigureFormater(
            string newInputFilePath = "D:\\text.txt",
            string newOutputFilePath = "D:\\Result.tsv",
            string newPrevFilePath = "",
            int newPrevOperatorPathNum = -1,
            int newPrevUserPathNum = -1,
            int newDefaultGuidCount = 4,
            string newDefaultColSeparator = "\t",
            string newDefaultRowSeparator = "\r\n",
            string newDefaultOutputHeader = "client_id\tpath\tsentence\tup_votes\tdown_votes\tage\tgender\taccent\r\n",
            int newDefaultUp_Votes = 2,
            int newDefaultDown_Votes = 0,
            AgeOption newDefaultAge = AgeOption.thirties,
            GenderOption newDefaultGender = GenderOption.male,
            string newDefaultAccent = "",
            bool createNewClientId = false,
            string newOperatorClientId = "",
            string newUserClientId = "")
        {
            _sourceFilePath = newInputFilePath;
            _resultFilePath = newOutputFilePath;
            _prevTsvFilePath = newPrevFilePath;

            _prevOperatorsPathNumber = newPrevOperatorPathNum;
            _prevUsersPathNumber = newPrevUserPathNum;
            _usePrevTsv = (!string.IsNullOrEmpty(_prevTsvFilePath) && File.Exists(_prevTsvFilePath)) || (newPrevOperatorPathNum > 0 && newPrevUserPathNum > 0);

            _defaultGuidCount = newDefaultGuidCount;
            _defaultColSeparator = newDefaultColSeparator;
            _defaultRowSeparator = newDefaultRowSeparator;
            _defaultOutputHeader = newDefaultOutputHeader;

            _defaultUp_Votes = newDefaultUp_Votes;
            _defaultDown_Votes = newDefaultDown_Votes;
            _defaultAge = newDefaultAge;
            _defaultGender = newDefaultGender;
            _defaultAccent = newDefaultAccent;

            if (createNewClientId)
            {
                CreateClientIds();
            }
            else
            {
                _currentOperatorClientId = string.IsNullOrEmpty(newOperatorClientId)? newOperatorClientId:_currentOperatorClientId;
                _currentUserClientId = string.IsNullOrEmpty(newUserClientId) ? newUserClientId : _currentUserClientId;
            }
        }

        public async Task CreateTsv()
        {
            var operatorSentences = new List<TsvDataString>();
            var userSentences = new List<TsvDataString>();

            try
            {
                // Get preverious numbers from *.tsv - if necessary
                if (_usePrevTsv)
                    await GetPreveriousFileNumbers();

                // Read the source file and filling collections
                using (var txtFile = new StreamReader(_sourceFilePath))
                {
                    var userPathCounter = _prevUsersPathNumber;
                    var operatorPathCounter = _prevOperatorsPathNumber;
                    var prePathNum = 0;

                    while (!txtFile.EndOfStream)
                    {
                        var line = await txtFile.ReadLineAsync();
                        line = line.Replace("\t\t", "\t");
                        var splitedLine = line.Split(_defaultColSeparator);
                        var isOperator = IsOperator(splitedLine[0]);

                        var curPathNum = GetNumberFromPath(splitedLine[0]);

                        var OutputTvsString = new TsvDataString()
                        {
                            Client_id = isOperator ? _currentOperatorClientId : _currentUserClientId,
                            Path = splitedLine[0],
                            Sentence = splitedLine[2],
                            Up_votes = _defaultUp_Votes,
                            Down_votes = _defaultDown_Votes,
                            Age = isOperator ? AgeOption.thirties : AgeOption.sixties,
                            Gender = _defaultGender,
                            Accent = _defaultAccent
                        };

                        //Get two collections with role's sentences
                        if (isOperator)
                        {
                            OutputTvsString.Path = _usePrevTsv ? CreatePath(curPathNum == prePathNum ? operatorPathCounter : operatorPathCounter += 2, OutputTvsString.Path) : OutputTvsString.Path;
                            operatorSentences.Add(OutputTvsString);
                        }
                        else
                        {
                            OutputTvsString.Path = _usePrevTsv ? CreatePath(curPathNum == prePathNum ? userPathCounter : userPathCounter += 2, OutputTvsString.Path) : OutputTvsString.Path;
                            userSentences.Add(OutputTvsString);
                        }

                        prePathNum = curPathNum;
                    }
                }

                // Write result file: first - operator, second - user
                using (var tsvFile = new StreamWriter(_resultFilePath))
                {
                    var sb = new StringBuilder();
                    sb.Append(_defaultOutputHeader);
                    foreach (var tsvRow in operatorSentences)
                    {
                        sb.Append($"{tsvRow.Client_id}{_defaultColSeparator}{tsvRow.Path}{_defaultColSeparator}{tsvRow.Sentence}{_defaultColSeparator}{tsvRow.Up_votes}{_defaultColSeparator}{tsvRow.Down_votes}{_defaultColSeparator}{tsvRow.Age.ToString()}{_defaultColSeparator}{tsvRow.Gender.ToString()}{_defaultColSeparator}{tsvRow.Accent}{_defaultRowSeparator}");
                    }
                    foreach (var tsvRow in userSentences)
                    {
                        sb.Append($"{tsvRow.Client_id}{_defaultColSeparator}{tsvRow.Path}{_defaultColSeparator}{tsvRow.Sentence}{_defaultColSeparator}{tsvRow.Up_votes}{_defaultColSeparator}{tsvRow.Down_votes}{_defaultColSeparator}{tsvRow.Age.ToString()}{_defaultColSeparator}{tsvRow.Gender.ToString()}{_defaultColSeparator}{tsvRow.Accent}{_defaultRowSeparator}");
                    }

                    await tsvFile.WriteAsync(sb.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error occures in method {nameof(CreateTsv)}.");
                throw ex;
            }
        }

        public async Task<IEnumerable<TsvDataString>> ReadTargetTsv(string tsvPath)
        {
            var tsvRows = new List<TsvDataString>();

            try
            {
                // Read the source file and filling collections
                using (var tsvFile = new StreamReader(tsvPath))
                {
                    await tsvFile.ReadLineAsync();
                    while (!tsvFile.EndOfStream)
                    {
                        var line = await tsvFile.ReadLineAsync();
                        var splitedLine = line.Split(_defaultColSeparator);

                        var OutputTvsString = new TsvDataString()
                        {
                            Client_id = splitedLine[0],
                            Path = splitedLine[1],
                            Sentence = splitedLine[2],
                            Up_votes = int.Parse(splitedLine[3]),
                            Down_votes = int.Parse(splitedLine[4]),
                            Age = splitedLine[5].Equals("thirties") ? AgeOption.thirties : AgeOption.sixties,
                            Gender = splitedLine[6].Equals("female") ? GenderOption.female : GenderOption.male,
                            Accent = splitedLine[7]
                        };

                        tsvRows.Add(OutputTvsString);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error occures in {nameof(ReadTargetTsv)} method!");
                throw ex;
            }

            return tsvRows;
        }

        /// <summary>
        /// Get operator's factor from Path name
        /// </summary>
        /// <param name="pathName">Format: [00.mp3] or [00_00.mp3] - used just first section</param>
        private bool IsOperator(string pathName)
        {
            try
            {
                if (pathName.Contains("_"))
                {
                    return (int.Parse(pathName.Split("_")[0]) & 1) != 0;
                }
                else
                {
                    return (int.Parse(pathName.Split(".")[0]) & 1) != 0;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error occires in {nameof(IsOperator)} method!");
                throw ex;
            }

        }

        /// <summary>
        /// Get preverious numbers of pathes from last *.tsv file
        /// </summary>
        /// <returns>Yohoho and bottle of rum!</returns>
        private async Task GetPreveriousFileNumbers()
        {
            try
            {
                var operatorSentences = new List<TsvDataString>();
                var userSentences = new List<TsvDataString>();

                using (var tsvFile = new StreamReader(_prevTsvFilePath))
                {
                    await tsvFile.ReadLineAsync();
                    while (!tsvFile.EndOfStream)
                    {
                        var line = await tsvFile.ReadLineAsync();
                        var splitedLine = line.Split(_defaultColSeparator);

                        var OutputTvsString = new TsvDataString()
                        {
                            Client_id = splitedLine[0],
                            Path = splitedLine[1],
                            Sentence = splitedLine[2],
                            Up_votes = int.Parse(splitedLine[3]),
                            Down_votes = int.Parse(splitedLine[4]),
                            Age = splitedLine[5].Equals("thirties") ? AgeOption.thirties : AgeOption.sixties,
                            Gender = splitedLine[6].Equals("female") ? GenderOption.female : GenderOption.male,
                            Accent = splitedLine[7]
                        };

                        var isOperator = IsOperator(OutputTvsString.Path);

                        if (isOperator)
                        {
                            operatorSentences.Add(OutputTvsString);
                        }
                        else
                        {
                            userSentences.Add(OutputTvsString);
                        }
                    }
                }

                _prevOperatorsPathNumber = GetNumberFromPath(operatorSentences.Last().Path);
                _prevUsersPathNumber = GetNumberFromPath(userSentences.Last().Path);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error int {nameof(GetPreveriousFileNumbers)} method!");
                throw ex;
            }
        }

        /// <summary>
        /// Get number from Path name
        /// </summary>
        /// <param name="pathName">Format: [00.mp3] or [00_00.mp3] - used just first section</param>
        /// <returns>Target number - the first section of Path name</returns>
        private int GetNumberFromPath(string pathName)
        {
            try
            {
                if (pathName.Contains("_"))
                {
                    return int.Parse(pathName.Split("_")[0]);
                }
                else
                {
                    return int.Parse(pathName.Split(".")[0]);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error occires in {nameof(GetNumberFromPath)} method!");
                throw ex;
            }
        }

        /// <summary>
        /// Create path string from any number
        /// </summary>
        /// <param name="NumberOne">Any number</param>
        /// <param name="oldPath">Old Path string</param>
        /// <returns>Path string in formst: [00][.mp3]</returns>
        private string CreatePath(int NumberOne, string oldPath)
        {
            var Has_ = oldPath.Contains("_");
            var parts = Has_ ? oldPath.Split("_") : oldPath.Split(".");

            NumberOne = Math.Abs(NumberOne);

            var tmpNUmberString = "00";
            if (NumberOne < 10)
            {
                tmpNUmberString = $"0{NumberOne}";
            }
            else
            {
                tmpNUmberString = $"{NumberOne}";
            }

            return Has_ ? $"{tmpNUmberString}_{parts[1]}" : $"{tmpNUmberString}.{parts[1]}";
        }
    }
}
