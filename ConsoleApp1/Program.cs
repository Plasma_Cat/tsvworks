﻿using System;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static async Task Main(string[] args)
        {
            try
            {
                //var TsvFormater = new TsvDataSetFormater(inputFilePath: "D:\\Bordanov\\939526_NOR_text.txt", prevTsvFilePath: "D:\\Bordanov\\dev.tsv");
                //var TsvFormater = new TsvDataSetFormater(inputFilePath: "D:\\Bordanov\\939526_NOR_text.txt");
                //await TsvFormater.CreateTsv();

                var txtWords = new WordsTxtFormater(sourceFilePath: "D:\\Result (1).tsv", prevTxtResultFilePath: "D:\\vocabulary.txt");
                await txtWords.GetWordList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            Console.ReadKey();
        }
    }
}
