﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public enum AgeOption
    {
        thirties,
        sixties
    }

    public enum GenderOption
    {
        female,
        male
    }

    /// <summary>
    /// Model contains following fields: client_id	path	sentence	up_votes	down_votes	age	gender	accent
    /// </summary>
    class TsvDataString
    {
        /// <summary>
        /// For example: Guid.NewGuid().ToString().Replace("-","") x4 times
        /// </summary>
        public string Client_id;
        public string Path;
        public string Sentence;
        public int Up_votes;
        public int Down_votes;
        public AgeOption Age;
        public GenderOption Gender;
        public string Accent;
    }
}