﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class WordsTxtFormater
    {
        private string _sourceFilePath;
        private string _resultFilePath;
        private string _prevTxtResultFilePath;

        /// <summary>
        /// Get new instance of WordsTxtFormater
        /// </summary>
        /// <param name="sourceFilePath">*.tsv from witch get sentences</param>
        /// <param name="resultFilePath">*.txt with result</param>
        /// <param name="prevTxtResultFilePath">*.txt if exist preverious result file, append to it new result</param>
        public WordsTxtFormater(string sourceFilePath = "", string resultFilePath = "D:\\Words from sentences.txt", string prevTxtResultFilePath = "")
        {
            _sourceFilePath = sourceFilePath;
            _resultFilePath = resultFilePath;
            _prevTxtResultFilePath = prevTxtResultFilePath;
        }

        public async Task GetWordList()
        {
            if (string.IsNullOrEmpty(_sourceFilePath) || !File.Exists(_sourceFilePath))
            {
                Console.WriteLine("Sours file path is empty or file not exists");
                return;
            }

            try
            {
                var tsvFormater = new TsvDataSetFormater();
                var tsvRows = await tsvFormater.ReadTargetTsv(_sourceFilePath);
                var sb = new StringBuilder();

                // Add preverious word if exists
                if (!string.IsNullOrEmpty(_prevTxtResultFilePath) || File.Exists(_prevTxtResultFilePath))
                {
                    // Read preverious file
                    var prevFile = await File.ReadAllTextAsync(_prevTxtResultFilePath);
                    sb.AppendLine(prevFile);
                }

                foreach (var tsvRow in tsvRows)
                {
                    var wordsFromSentense = tsvRow.Sentence.Replace(".", "").Replace(",", "").Replace("!", "").Replace("?", "").Replace("-", "").Replace(".", "").Replace(" ", "\r\n");
                    sb.AppendLine(wordsFromSentense);
                }

                // Write result
                await File.WriteAllTextAsync(_resultFilePath, sb.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine($"En error has occured in {nameof(GetWordList)} method!");
                throw ex;
            }
        }
    }
}
